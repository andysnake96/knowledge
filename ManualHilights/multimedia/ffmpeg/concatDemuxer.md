#CONCAT

Virtual concatenation script demuxer.

This demuxer reads a list of files and other directives from a text file and
demuxes them one after the other, as if all their packets had been muxed
together.

The timestamps in the files are adjusted so that the first file starts at 0
and each next file starts where the previous one finishes. Note that it is
done globally and may cause gaps if all streams do not have exactly the same
length.

All files must have the same streams (same codecs, same time base, etc.).

The duration of each file is used to adjust the timestamps of the next file:
if the duration is incorrect (because it was computed using the bit-rate or
because the file is truncated, for example), it can cause artifacts. The
@code{duration} directive can be used to override the duration stored in
each file.

##Syntax
The script is a text file in extended-ASCII, with one directive per line.
Empty lines, leading spaces and lines starting with '#' are ignored. The
following directive is recognized:


###path:
Path to a file to read; special characters and spaces must be escaped with
backslash or single quotes.

All subsequent file-related directives apply to that file.

###ffconcat version 1.0:
Identify the script type and version. It also sets the @option{safe} option
to 1 if it was -1.

To make FFmpeg recognize the format automatically, this directive must
appear exactly as is (no extra space or byte-order-mark) on the very first
line of the script.

###duration - var:dur
Duration of the file. This information can be specified from the file;
specifying it here may be more efficient or help if the information from the
file is not available or accurate.

If the duration is set for all files, then it is possible to seek in the
whole concatenated video.

###inpoint @var{timestamp}}
In point of the file. When the demuxer opens the file it instantly seeks to the
specified timestamp. Seeking is done so that all streams can be presented
successfully at In point.

This directive works best with intra frame codecs, because for non-intra frame
ones you will usually get extra packets before the actual In point and the
decoded content will most likely contain frames before In point too.

For each file, packets before the file In point will have timestamps less than
the calculated start timestamp of the file (negative in case of the first
file), and the duration of the files (if not specified by the @code{duration}
directive) will be reduced based on their specified In point.

Because of potential packets before the specified In point, packet timestamps
may overlap between two concatenated files.

###outpoint @var{timestamp}}
Out point of the file. When the demuxer reaches the specified decoding
timestamp in any of the streams, it handles it as an end of file condition and
skips the current and all the remaining packets from all streams.

Out point is exclusive, which means that the demuxer will not output packets
with a decoding timestamp greater or equal to Out point.

This directive works best with intra frame codecs and formats where all streams
are tightly interleaved. For non-intra frame codecs you will usually get
additional packets with presentation timestamp after Out point therefore the
decoded content will most likely contain frames after Out point too. If your
streams are not tightly interleaved you may not get all the packets from all
streams before Out point and you may only will be able to decode the earliest
stream until Out point.

The duration of the files (if not specified by the @code{duration}
directive) will be reduced based on their specified Out point.

###file_packet_metadata @var{key=value}}
Metadata of the packets of the file. The specified metadata will be set for
each file packet. You can specify this directive multiple times to add multiple
metadata entries.

###stream
Introduce a stream in the virtual file.
All subsequent stream-related directives apply to the last introduced
stream.
Some streams properties must be set in order to allow identifying the
matching streams in the subfiles.
If no streams are defined in the script, the streams from the first file are
copied.

###exact_stream_id @var{id}
Set the id of the stream.
If this directive is given, the string with the corresponding id in the
subfiles will be used.
This is especially useful for MPEG-PS (VOB) files, where the order of the
streams is not reliable.

##Options

This demuxer accepts the following option:

###safe
If set to 1, reject unsafe file paths. A file path is considered safe if it
does not contain a protocol specification and is relative and all components
only contain characters from the portable character set (letters, digits,
period, underscore and hyphen) and have no period at the beginning of a
component.

If set to 0, any file name is accepted.

The default is 1.

-1 is equivalent to 1 if the format was automatically
probed and 0 otherwise.

###auto_convert
If set to 1, try to perform automatic conversions on packet data to make the
streams concatenable.
The default is 1.

Currently, the only conversion is adding the h264_mp4toannexb bitstream
filter to H.264 streams in MP4 format. This is necessary in particular if
there are resolution changes.

###segment_time_metadata
If set to 1, every packet will contain the @var{lavf.concat.start_time} and the
@var{lavf.concat.duration} packet metadata values which are the start_time and
the duration of the respective file segments in the concatenated output
expressed in microseconds. The duration metadata is only set if it is known
based on the concat file.
The default is 0.

##Examples

Use absolute filenames and include some comments:
@example
# my first filename
file /mnt/share/file-1.wav
# my second filename including whitespace
file '/mnt/share/file 2.wav'
# my third filename including whitespace plus single quote
file '/mnt/share/file 3'\''.wav'
@end example

@item
Allow for input format auto-probing, use safe filenames and set the duration of
the first file:
@example
ffconcat version 1.0

file file-1.wav
duration 20.0

file subdir/file-2.wav
@end example
@end itemize

