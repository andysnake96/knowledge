introMail
========
Buongiorno Andrea,

sono Emiliano Ingrassia di Epigenesys s.r.l..

Come concordato con Betti ti invio un po' di materiale
per introdurti alla programmazione FPGA con il linguaggio
ad "alto livello" HLS. E' fondamentalmente linguaggio C++
con delle pragma proprietarie per il compilatore.

Per una introduzione generica al mondo FPGA puoi partire da Wikipedia:

https://en.wikipedia.org/wiki/Field-programmable_gate_array

Come scoprirai, questi oggetti possono essere programmati a partire
da diversi linguaggi. I linguaggi "assembly" di questi oggetti sono i
linguaggi HDL. Per adesso non ci interessa trattarli.

Per la programmazione HLS ti do due puntatori:
1) "Parallel programming for FPGAs" di Kastner;
2) La guida ufficiale del compilatore Vitis HLS di Xilinx.

Il libro di Kastner e' open source e puoi rigenerarlo liberamente dai
sorgenti:
https://github.com/KastnerRG/pp4fpgas

La guida del compilatore Xilinx la trovi qui:
https://docs.xilinx.com/viewer/book-attachment/omhD00vckDB4yVymm_JqkA/bkEGUt1hjbGN_N9lk~k_eQ

In questo repository trovi un po' di codice di esempio:

https://github.com/Xilinx/Vitis-HLS-Introductory-Examples

Xilinx e' il maggior vendor FPGA al mondo (recentemente acquisito da AMD).
L'altro grande era Altera (che qualche anno fa e' stato acquisito da Intel).

La programmazione HLS (C/C++ su FPGA) e' un concetto generale:

https://en.wikipedia.org/wiki/High-level_synthesis

Esistono dunque diversi compilatori ma non esiste una sintassi generica.
Il nostro target e' costituito dagli acceleratori Xilinx, quindi siamo
interessati
alla sintassi del loro compilatore. Da qualche anno e' anche possibile
programmare questi oggetti con il linguaggio OpenCL. Anche qui pero'
sussiste per adesso una differenza per le pragma vendor-dependent.
Per adesso non ci interessa approfondire la questione.

Per qualsiasi dubbio puoi scrivermi a questa mail.

Buona giornata e buon lavoro.

E. Ingrassia